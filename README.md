# Mosig InfoViz 2020 Zoo

A inoffical repository to collect student's answers to the data visualization project in the InfoViz class taught at ENSIMAG in 2020. See the [offical page](http://iihm.imag.fr/blanch/teaching/infovis/).

## How can I see the all visualizations?

Go to [https://halmm.gricad-pages.univ-grenoble-alpes.fr/infoviz-2020-zoo/](https://halmm.gricad-pages.univ-grenoble-alpes.fr/infoviz-2020-zoo/).

## How to can my own visualization to the zoo?

The following steps assume that you created your visualization can be viewed in a browser (e.g. it was made with HTML and JavaScript). If this is not the case, feel free to use this repository anyway to store your files and some instructions on how to view the visualization.

1. Create a folder under `visualizations/` with an html file containing the visualization and all your required code. Note that the data and the D3 libary are already present in the folders `data` and `vendor`, respectivel.y

2. Edit `index.html` and add a link to your html file.
