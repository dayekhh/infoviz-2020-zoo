const default_margin = {top: 100, right: 100, bottom: 100, left: 100, middle: 100};
const color_scheme = ["#66c2a5","#fc8d62","#8da0cb","#e78ac3","#a6d854","#ffd92f","#e5c494","#b3b3b3"]

function OneToNDiagram(div, w, h) {
    this.width = w
    this.height = h
    this.margin = default_margin
    this.div = div
    this.computeDistances()
}

OneToNDiagram.prototype = {
    constructor: OneToNDiagram,
    svg: function(svg) {
        if (svg) {
            this.svg = svg;
            return this;
        }
        else return this.svg;
    },
    getOrSetMargin: function(margin) {
        if (margin) {
            this.margin = {...this.margin, ...margin}
            this.computeDistances()
            return this
        } else  {
            return this.margin
        }
    },
    setTooltipContent: function(t) {
        this.toolTipContent = t
    },
    getOrSetEntities: function(data, getValue, getGroup) {
        this.entities = data;
        if (getValue) this.getEntityValue = getValue;
        if (getGroup) this.getGroupOf  = getGroup;
    },
    getOrSetGroups: function(data, getId) {
        this.groups = data;
        if (getId) this.getGroupId = getId;
    },
    vAxisLabel: function(label) {
        this.svg.select("#left_v_axis_label").text(label)
    },
    leftHAxisLabel: function(label) {
        this.svg.select("#left_h_axis_label").text(label)
    },
    rightHAxisLabel: function(label) {
        this.svg.select("#right_h_axis_label").text(label)
    },
    draw_frame: function () {
        this.draw_svg()
        this.build_scales()
    },
    draw_data: function () {
        this.sort_data()
        this.populate_axes()
        this.plot_bars()
        this.plot_links()
    },
    draw_svg: function() {
        let svg = this.div.append("svg")
        this.svg = svg
            .attr('width', this.width)
            .attr('height', this.height)
            .insert('g')
            .attr('transform', `translate(${this.margin.left},${this.margin.top})`);
        this.bg = this.svg.append('rect')
            .attr("class", "frame")
            .attr('width', this.inner_width)
            .attr('height', this.inner_height);
        this.left_space = this.svg.append("g")
        this.right_space = this.svg
            .append('g')
            .attr('transform', `translate(${this.middle_right},0)`)
        this.middle_space = this.svg
            .append('g')
            .attr('transform', `translate(${this.middle_left}, 0)`)
        this.middle_space.append('rect')
            .attr('width', this.margin.middle)
            .attr('height', this.inner_height)
            .attr('class', 'frame')
        this.right_space
            .append("rect")
            .attr("width", this.inner_width - this.middle_right)
            .attr("height", this.inner_height)
            .attr("opacity", 0)
            .on('mouseout', e =>  {this.svg.selectAll(".data-element").classed("selected", true)})

        this.tooltip = d3.select("body")
            .append("div")
            .style("opacity", 1)
            .attr("class", "tooltip")
            
    },
    computeDistances: function() {
        this.inner_width = this.width - this.margin.left - this.margin.right,
        this.inner_height = this.height - this.margin.top - this.margin.bottom;
        this.middle_left = (this.inner_width - this.margin.middle) / 2;
        this.middle_right = (this.inner_width + this.margin.middle) / 2;
        this.right = this.width - this.margin.right;
        this.left = this.margin.left;
    },
    build_scales: function() {
        this.entity_scale = d3.scaleLinear()
            .range([this.inner_height, 0])
            .clamp(true);
        this.left_v_axis = this.svg
            .append('g')
            .attr('transform', `translate($0,0)`)
        this.svg.append("text")
            .attr("id", "left_v_axis_label")
            .attr("class", "axis-label")
            .attr("y", -45)
            .attr("text-anchor", "middle")
            .attr("x", -this.inner_height / 2)
            .attr("transform", "rotate(-90)")
  
        this.entity_value_scale = d3.scaleLinear()
            .range([this.middle_left, 0])
            .clamp(true);
        this.left_h_axis = this.svg
            .append('g')
            .attr('transform', 'translate(0,0)')
        this.svg.append("text")
            .attr("id", "left_h_axis_label")
            .attr("class", "axis-label")
            .attr("text-anchor", "middle")
            .attr("x", this.middle_left / 2)
            .attr("y", -30)
        
        this.right_v_axis = this.right_space
            .append('g')
        this.group_value_scale= d3.scaleLinear()
            .range([0, this.inner_width - this.middle_right])
            .clamp(true);
        this.right_h_axis = this.right_space
            .append('g')
         
        this.right_space.append("text")
          .attr("id", "right_h_axis_label")
          .attr("class", "axis-label")
          .attr("text-anchor", "middle")
          .attr("x", (this.inner_width - this.middle_right) / 2)
          .attr("y", -30)
    },
    populate_axes: function() {
        if (this.entities) {
            this.entity_scale.domain([0, this.entities.length - 1]);
            this.entity_value_scale
                .domain([0, d3.max(this.entities, this.getEntityValue)])
                .nice()
            this.left_v_axis.call(d3.axisLeft(this.entity_scale));
            this.left_h_axis.call(d3.axisTop(this.entity_value_scale));
        }
        if (this.groups) {
            this.group_value_scale
                .domain([0, d3.max(this.groups, g => g._value)])
                .nice()           
            this.right_h_axis.call(d3.axisTop(this.group_value_scale))
        }
        this.bar_height = Math.abs(this.entity_scale(1) - this.entity_scale(0))
    },
    sort_data: function() {
        this.entities.forEach((d, i) => d._id = i)
        this.entities.sort((a, b) => this.getEntityValue(a) - this.getEntityValue(b))
        
        let = reverse_mapping = new Map(this.groups.map(g => [this.getGroupId(g), []]))
        this.entities.forEach(e => {
            reverse_mapping.get(this.getGroupOf(e)).push(e)
        });
        this.groups.forEach((g, i) => {
            g._id = this.getGroupId(g)
            g._members = reverse_mapping.get(this.getGroupId(g))
            g._size = g._members.length
            g._sum = g._members.map(this.getEntityValue).reduce((a,b) => a + b)
            g._value = g._sum / g._size
        })

        this.groups.sort((a, b) => a._value - b._value)
        let members_cumulated = 0
        this.groups.forEach((g, i) => {
            g._members_cumulated = members_cumulated
            members_cumulated += g._size
            g._color = color_scheme[i % color_scheme.length]
            g._members.forEach((e, i ) => {
                e._color = g._color
                e._target = g._members_cumulated + i + 1
            })
        });
    },
    plot_bars: function() {
        this.entity_bars = this.left_space.selectAll("rect")
            .data(this.entities)
            .enter()
            .append("rect")
            .attr("x",(d, i) =>  this.entity_value_scale(this.getEntityValue(d)))
            .attr("y", (d, i) => this.entity_scale(i))
            .attr("width", (d,i) => this.middle_left - this.entity_value_scale(this.getEntityValue(d)))
            .attr("height", this.bar_height)
            .attr("fill", d => d._color)
            .attr("class", "selected data-element")
       
        var mouseover = function(e, d) {
            this.svg.selectAll(".data-element").classed("selected", x => x === d || this.getGroupOf(x) == this.getGroupId(d))
            d3.select(".tooltip").style("opacity", 1)
          }
        var mousemove = function(event, d) {
            let [x, y] = d3.pointer(event, d, d3.select(body).node())
            d3.select(".tooltip")
            .style("left", event.pageX + 10 + "px")
            .style("top", event.pageY + "px")
            .style("border-color", d._color)
            .html(this.toolTipContent(d))   
        }
        var mouseleave = function(e, d) {
            let [x, y] = d3.pointer(e, this.right_space.node())
            if (x < 0 || x > this.inner_width - this.middle_right || y < 0 || y > this.inner_height) {
            this.svg.selectAll(".data-element").classed("selected", true)
            }
            d3.select(".tooltip").style("opacity", 0)
        }

        this.group_bars = this.right_space.selectAll("rect")
            .data(this.groups)
            .enter()
            .append("rect", this.getGroupId)
            .attr("x", 0)
            .attr("y", (d, i) => this.entity_scale(d._members_cumulated + d._size))
            .attr("width", d => this.group_value_scale(d._value))
            .attr("height", d => Math.abs(this.entity_scale(d._size) - this.entity_scale(0)))
            .attr("fill", d => d._color)
            .attr("class", "selected data-element")
            .on('mouseover', mouseover.bind(this))
            .on('mousemove', mousemove.bind(this))
            .on('mouseout', mouseleave.bind(this))
    },
    plot_links: function() {
        var linkGen = d3.linkHorizontal()
            .x(d => d[0])
            .y(d => this.entity_scale(d[1]) + this.bar_height / 2)
            .source((d, i) => [0, i])
            .target((d, i) => [this.margin.middle, d._target])
        this.paths = this.middle_space
            .selectAll("path")
            .data(this.entities, e => e._id)
            .enter()
            .append("path")
            .attr("d", linkGen)
            .attr("fill","none")
            .attr("stroke", d => d._color)
            .attr("stroke-width", this.bar_height)
            .attr("class", "selected data-element")
    },

};
